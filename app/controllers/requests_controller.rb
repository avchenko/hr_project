class RequestsController < ApplicationController
  
  def create
    @employee = Employee.find(params[:employee_id])
    @request = @employee.requests.create(request_params)
    redirect_to employee_path(@employee) 
  end  
  
  def destroy
    @employee = Employee.find(params[:employee_id])
    @request = @employee.requests.find(params[:id])
    @request.destroy
    redirect_to employee_path(@employee)
  end
  
    
  private
    def request_params
      params.require(:request).permit(:hol_start_date, :hol_end_date, :comment)
    end

end
