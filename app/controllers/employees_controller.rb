class EmployeesController < ApplicationController
  before_action :authenticate_user!, except: [:create, :edit] 
  
  def index
    # to show all employees under /index path
    #search by name and return results
        if params[:search]
          @employees = Employee.search(params[:search])
        else 
          @employees = Employee.all
        end
  end
  

  
  
 # to show 1 employee 
  def show
    @employee = Employee.find(params[:id])
  end

  def new
    @employee = Employee.new
  end
  
  def edit
    @employee = Employee.find(params[:id])
  end

  # action to create an employee
  def create
    
    @employee = Employee.new(employee_params)
 
      if @employee.save
        redirect_to @employee
      else
        render 'new'
      end

  end
  
  # to edit employee details
  def update
    @employee = Employee.find(params[:id])
 
      if @employee.update(employee_params)
        redirect_to @employee
      else
        render 'edit'
      end
  end
 
 # to remove employee
  def destroy
      @employee = Employee.find(params[:id])
      @employee.destroy
 
      redirect_to employees_path
  end

  # whitelist of controller paramaters to allow them access
  private
    def employee_params
      params.require(:employee).permit(:name, :email, :address, :start_date)
    end

end
