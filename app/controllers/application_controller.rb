class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  # to override pdefault devise path for "after sign in/up"
  def after_sign_in_path_for(resource)
    employees_path
  end
  
  def after_sign_up_path_for(resource)
    employees_path
  end
end