class Request < ActiveRecord::Base
  belongs_to :employee
  validates :hol_start_date, :hol_end_date, presence: true
  
end
