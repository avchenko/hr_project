class Employee < ActiveRecord::Base
  has_many :requests, dependent: :destroy
  validates :name, presence: true,
                    length: { minimum: 6 }
  validates :email, presence: true,
                    length: { minimum: 8 }
  
      # It returns the employees whose name contain one or more words inc. in search
  def self.search(query)
    # where(:name, query) -> This would return an exact match of the query
    where("name like ?", "%#{query}%") 
  end

end
