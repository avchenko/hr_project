class CreateRequests < ActiveRecord::Migration
  def change
    create_table :requests do |t|
      t.date :hol_start_date
      t.date :hol_end_date
      t.text :comment
      t.references :employee, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
